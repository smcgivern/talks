Some talks.

See the latest versions at
[https://smcgivern.gitlab.io/talks/](http://smcgivern.gitlab.io/talks/).

To view locally, run `python -m SimpleHTTPServer` then load
[http://localhost:8000/](http://localhost:8000/).
