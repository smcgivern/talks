# Remote working

Theme - considerations for remote work
Return to - not perfect, work on the downsides
Hook - The big numbers in savings

- NOT about being self-employed
- Talk is about:
  - What it's to be a remotee in practice (2 projects)
    - Sounds cool (and is) but it's not 100% gold
    - How our day looks
    - How our teams work
  - More generally
    - Considerations for employees
    - Considerations for businesses

# Greg

expand on "evangelist", can be a polarising word :)

# Sean

# Foreman

# GitLab

# GitLab Work in the open

# Foreman Work in the open

Mock Sean for having "open" and "Slack" in the same slide :)

# General Work in the open

Inner-sourcing is becoming more popular, much of Red
Hat's consulting arm is employed in teaching big
companies how to be "open" internally

TZs relevant even when people are travelling

Becomes a habit - eg Greg uses an etherpad to organise
the weekly shopping order with his wife

Actually working in an "open" fashion today - separate
trains yet collaboration on the material :)

# No Metrics

This is important for employers and employees alike -
breaking the "hours" view is *really*  hard.

# Foreman Social avenues

Note the timezone distribution. Also point out this isn't
just Red Hat - we're open source after all.

# GitLab Social avenues

Met people after 6 months in-person, but already
familiar

# GitLab typical day

# Foreman typical day

Not a dev any more, so hard to predict what needs dealing
with.

# Upsides

Productivity (70% reported improved productivity)
- work when you’re ready to, not when you’re expected to
- no cubicle drivebys
- no commuting
Flexibility - errands, emergencies, etc.
  - 80% improved morale
  - 82% reduced stress

PGI, 2014: http://www.slideshare.net/PGi/state-of-telecommuting-2014-pgi-report/1
US-only data: http://globalworkplaceanalytics.com/telecommuting-statistics & http://globalworkplaceanalytics.com/cut-oil

# Downsides

Isolation
  - 38% worsened collaboration
  - 30% worsened meetings
 - getting out of the house
Seren. - hallway track, conversations at lunch

So, clear benefits, not 100% perfect, but no real
surprise that employees like remote work. What about
employers?

# Upsides employer

* Most business hire with ~20mile radius
* Savings based on office real estate, contract perks,
  insurance etc.
* Not self-employed, thats down 4%

US-only data: http://globalworkplaceanalytics.com/telecommuting-statistics & http://globalworkplaceanalytics.com/cut-oil

# Downsides employer

* does *need* to be a policy, don't leave it to
  managers
  * Certainty for employees, e.g. childcare
* We're assuming employers already like the cost
  savings and happier, more prouctive employees
  * give ppl good tools to be part of the team
  * picking up the phone / vid call should always be
    acceptable
  * Difficult mindset to get to when only a few people
    are remote
* Perks only available to employees in the office
  - Childcare/creche, free lunches at product launch
    career progression etc

# Cisco quote

"Cisco  Systems, the computing networking company that
adopted a telecommuting  policy in 1993, calculated
that it realized $195 million in productivity  gains
from its approach in just one year.Still,  there are
downsides. The Stanford study found that the rate at
which  home-based workers were promoted dropped by 50
percent, seeming to confirm the cliché “out of sight,
out of mind.” " -

http://www.nytimes.com/2013/03/03/opinion/sunday/working-from-home-vs-the-office.html

# ONS quote

"Given the loss of personal wellbeing generally
associated with  commuting, the results suggest that
other factors such as higher income  or better housing
may not fully compensate the individual commuter for
the negative effects associated with travelling to work
and that people  may be making sub-optimal choices.
This result is consistent with the findings of previous
studies such as Stutzer and Frey (2008). This is
potentially important information both for those who
commute,  particularly for an hour or more, and for
their employers."

on making bad choices, ONS 2014
