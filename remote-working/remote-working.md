class: center, middle

# Remote working

Greg Sutcliffe (Red Hat)

Sean McGivern (GitLab)

---

# ![](sean.jpg) About Sean

- Rubiyst, wannabe Lisper
- Work on discussion / CR at GitLab
- [@mcgivernsa][mcgivernsa]
- [sean@mcgivern.me.uk](mailto:sean@mcgivern.me.uk)

---

# ![](greg.jpg) About Greg

- Hacker, speaker, FOSS/tech evangelist
- Community manager for Red Hat
- Remote worker for over 4 years
- [@gwmngilfen][gwmngilfen]
- [gwmngilfen@emeraldreverie.org](mailto:gwmngilfen@emeraldreverie.org)

---

# ![](foreman.png) Foreman (Red Hat)

- Server lifecycle management project
- Red Hat - ~6,000 employees, ~30% remote
- Foreman team - ~40 employees, ~25% remote
- US, Israel, Czech Republic, and remote
- No paid version - but forms the upstream of Red Hat Satellite 6

---

# ![](gitlab.svg) GitLab

- Self-hosted version control
- Fully remote - marketing, sales, etc.
- ~130 employees (depending on the day!)
- A bunch of countries
- FOSS Community Edition, free GitLab.com
- Paid for by Enterprise Edition, GitHost.io, and others

---

class: gitlab-team

---

# ![](gitlab.svg) Work in the open

- Everything I work on is a GitLab issue, visible on GitLab.com
- Our handbook is publicly available, too
- Calendar availability on Calendly

---

# ![](gitlab.svg) Work in the open

.width.w60[![](slack-stats.png)]

---

# ![](foreman.png) Work in the open

- Same, but Redmine for issues
- IRC \#theforeman on Freenode
- Mailing lists on Google Groups

---

# Work in the open

- Innersourcing has a lot of the benefits
  - Don't have to be a FOSS project
- Async forces you to write things down
  - That's good! That's documentation!
  - Inclusive when you work across TZs
- Lowers the &ldquo;bus factor&rdquo;

---

# No metrics!

- OK, they can be useful
- Do not track time
- Remote work means getting work done, not working a set number of hours

---

# ![](foreman.png) Social avenues

- Virtual coffee 9:30 am UK time - everyone except the US
- \#theforeman-dev has non-dev chat, too
- One-to-one hangouts
- (Conferences, infrequently)

---

# ![](foreman.png) #theforeman-dev

.width.w100[![](theforeman-dev.png)]

---

# ![](gitlab.svg) #working-on

.width.w80[![](working-on.png)]

---

# ![](gitlab.svg) Team call

- Every day, 8:30 am PST
- Recorded, so not mandatory
- Agenda available to the whole company
- What did you do at the weekend?

---

# ![](gitlab.svg) Social avenues

- \#random hangout - drop in any time
- Coffee break calls - schedule with anyone
- Travel grants for conferences, meeting team members, etc.

---

# ![](gitlab.svg) A typical day

- Review any new MRs assigned to me
- Any other todos from issues / MRs
- Review community contributions
- Lunch!
- Work on features / bugs for next release

---

# ![](foreman.png) A typical day

- Varied!
- Lots of email - planning, ordering, discussing
- Video recording / broadcasting
- Collecting / analysing community metrics

---

# Employee - upsides

- Productivity / efficiency (70%)
- Flexibility (80%)
- Working in the open
  - Clarifies thinking
  - Makes handover easier
- Cost!
  - Telecommuters save $2k - $7k / year

.fn[sources: [PGI 2014 study](http://www.slideshare.net/PGi/state-of-telecommuting-2014-pgi-report/1) &
[GlobalWorkplaceAnalytics](http://globalworkplaceanalytics.com/telecommuting-statistics)]

---

# Employee - downsides

- Isolation (38%)
- Serendipity - (34%)
- Burnout - put the laptop down!
  - &ldquo;Tomorrow's me will fix it&rdquo;
- Career progression (21%)

.fn[source: [PGI 2014 study](http://www.slideshare.net/PGi/state-of-telecommuting-2014-pgi-report/1)]

---

# Employer - upsides

* A typical business would save <br> $11k / person / year
* Hire where the talent is
* Regular work-at-home up 103% since 2005

.fn[source: [GlobalWorkplaceAnalytics](http://globalworkplaceanalytics.com/telecommuting-statistics)]

---

# Employer - downsides

* Need to consider how to implement a remote work policy
* How to mitigate the employee downsides
  * Provide equipment / pay for internet
  * Encourage communication
  * Remote mindset even in the office
  * Watch out for imbalances

---

# Cisco - cost vs talent

* Cisco adopted telecommuting in 1993
  * Realized $195mil in productivity gains *alone* in 1 year
  * Rate of promotion of home-based workers dropped 50%
  * &ldquo;Out of sight, out of mind&rdquo;

.fn[source: [NY Times 2013](http://www.nytimes.com/2013/03/03/opinion/sunday/working-from-home-vs-the-office.html)]

---

# Final word - ONS

.open-quote[&ldquo;]Given the loss of personal wellbeing generally
associated with commuting, the results suggest that
other factors such as higher income or better housing
may not fully compensate the individual commuter for
the negative effects associated with travelling to work
and that people may be making sub-optimal choices.&rdquo;

.fn[source: [ONS 2014](http://webarchive.nationalarchives.gov.uk/20160105160709/http://www.ons.gov.uk/ons/dcp171766_351954.pdf)]

---

class: center, middle

# Questions?

[@gwmngilfen][gwmngilfen]

[@mcgivernsa][mcgivernsa]

Hack day tomorrow!

[gwmngilfen]: https://twitter.com/gwmngilfen
[mcgivernsa]: https://twitter.com/mcgivernsa
