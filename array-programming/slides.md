class: center, middle

# Array programming

A short J tutorial

---

# What to expect

- Brevity

---

# What to expect

- Brevity
    - No, really

---

# What to expect

- Brevity
    - No, really, programs in these languages are extremely short
- Quick background on array programming in general
- Dive into J

---

# Why?

- It's fun
- Influence the way we write code in other languages
    - Main data structure is an array
    - Automatic application to different shapes of array
    - Strong focus on notation

---

# Big idea: notation

Some classic papers by Kenneth Iverson:

- [The Description of Finite Sequential Processes][dfsp]
- [Notation as a Tool of Thought][ntt]
    - Also introduces APL
- [Operators and Functions][of]

[ntt]: https://www.jsoftware.com/papers/tot.htm
[dfsp]: https://www.jsoftware.com/papers/DFSP.htm
[of]: https://www.jsoftware.com/papers/opfns.htm

---

# [APL Game of Life][yt]

```
r ← (3 3 ⍴ ⍳ 9) ∊ 2 3 4 5 8
R ← ¯1 ⊖ ¯2 ⌽ 5 7 ↑ r
life←{⊃1 ⍵ ∨.∧ 3 4 = +/ +/ 1 0 ¯1 ∘.⊖ 1 0 ¯1 ⌽¨ ⊂⍵}
gen←{(life⍣⍵)⍺}
R∘gen¨ ⍳4

┌─────────────┬─────────────┬─────────────┬─────────────┐
│0 0 0 0 0 0 0│0 0 0 1 0 0 0│0 0 1 1 0 0 0│0 1 0 0 0 0 0│
│0 0 1 1 1 0 0│0 0 1 1 0 0 0│0 0 1 1 1 0 0│0 1 0 0 1 0 0│
│0 0 1 0 0 0 0│0 1 0 0 1 0 0│0 1 0 0 1 0 0│0 1 0 0 1 0 0│
│0 0 1 1 0 0 0│0 0 1 1 0 0 0│0 0 1 1 0 0 0│0 1 0 0 1 0 0│
│0 0 0 0 0 0 0│0 0 0 0 0 0 0│0 0 1 1 0 0 0│0 1 0 0 1 0 0│
└─────────────┴─────────────┴─────────────┴─────────────┘
```

[yt]: https://www.youtube.com/watch?v=a9xAKttWgP4

---

# J and K

- ASCII-based descendants of APL
- J
    - Created by Kenneth Iverson after [retiring from paid employment]
    - Large standard vocabulary
    - Dual-licensed (FOSS / commercial) [since 2011][j-foss]
- K (and Q)
    - Created by Arthur Whitney, commercial languages
    - Much smaller, more flexible standard vocabulary
    - Known for being very fast
    - Like Perl, K5 is not K6 is not K7 - latest is [Shakti K]

[retiring from paid employment]: https://web.archive.org/web/20040812193452/http://home1.gte.net/res057qw/APL_J/IversonAPL.htm
[j-foss]: https://github.com/openj/core#j-goes-gnu
[Shakti K]: https://shakti.com/tutorial/

---

# J by example

```
   ] a =: 1 + i. 12
1 2 3 4 5 6 7 8 9 10 11 12
   +/ a NB. sum
78
   (+/ % #) a NB. mean
6.5
```

- `a` is a **noun**
- `=:` is a **copula** (global assignment; `=.` is local)
- `+` is a **verb** (so are `]`, `i.`, `%`, and `#`)
- `/` is an **adverb**
- `+/ % #` is a **fork**
- All of the above items are **words**
- `NB.` is a **comment**
- Each line is a **sentence**

---

# Basics


```
   ] a =: 1 + i. 12
1 2 3 4 5 6 7 8 9 10 11 12
   i. 12 NB. first twelve non-negative integers
0 1 2 3 4 5 6 7 8 9 10 11
   1 + i. 12
1 2 3 4 5 6 7 8 9 10 11 12
   (i. 12) + i. 12
0 2 4 6 8 10 12 14 16 18 20 22
   2 * i. 12
0 2 4 6 8 10 12 14 16 18 20 22
```

- No precedence, just right-to-left execution
- `i.` is a verb to which we pass the noun `12`
- `+` is also a verb that we call here with two noun arguments (left and right)
- Operations apply to the whole array
- What does `(i. 3) + i. 12` do?

---

# [J monad tutorial][monad]

A verb with only a right (`y`) argument. A dyad has left (`x`) and right
arguments.

```
   3 %: 27 NB. dyad; cube root
3
   %: 81 NB. monad; square root
9
```

[monad]: https://code.jsoftware.com/wiki/Vocabulary/Glossary#Monad

---

# Assignment


```
   ] a =: 1 + i. 12
1 2 3 4 5 6 7 8 9 10 11 12
   a =: 1 + i. 12
   a
1 2 3 4 5 6 7 8 9 10 11 12
   ] a
1 2 3 4 5 6 7 8 9 10 11 12
   ] a =: 2
2
   a
2
```

- Assignment doesn't display the value by default
- It does return the value
    - Try `2 = (a =: 2)`
- `]` is identity
    - (As is `[`; they also have another use we don't need to get into here)

---

# Adverbs

```
   ] a =: 1 + i. 12
1 2 3 4 5 6 7 8 9 10 11 12
   +/ a NB. sum
78
   */ a NB. product
479001600
   >./ a NB. greatest
12
```

- `/` is an adverb that works like reduce
- `+`, `*`, and `>.` are all verbs
- It inserts the verb on its left between every item of the noun on its right
- What does it do on a table?

---

# Shape

```
   ] b =: 3 4 $ a
1  2  3  4
5  6  7  8
9 10 11 12
   +/ b
15 18 21 24
```

- `$` reshapes a noun: here, from a 12-element list to a 3x4 table
- Each of the numbers is an atom
- `+/` on a table gives column sums
- Two methods for row sums:
    - Transpose the table with `|:` - `+/ |: y`
    - Operate on the 1-cells with explicit rank: `+/"1 y`

---

# Rank

```
   +/"1 b
10 26 42
```

- `"` is a conjunction
- It makes the verb on its left have the rank given by its right argument
- By default, `+/` has infinite rank
- Use `u b. 0`, where `u` is a verb, to inspect its rank

---

# Rank

```
   +/ b
15 18 21 24
   +/"1 b NB. conjoin +/ and 1
10 26 42
   +/ (1 2 3 4) , (5 6 7 8) ,: (9 10 11 12) NB. +/ b
15 18 21 24
   (+/ 1 2 3 4) , (+/ 5 6 7 8) , (+/ 9 10 11 12) NB. +/"1 b
10 26 42
```

- What do you expect `+/"0` to do?
- Or `+/"2`? `+/"3`?

---

# Conjunctions

```
   +/"1 b NB. conjoin +/ and 1
10 26 42
   +: a NB. double
2 4 6 8 10 12 14 16 18 20 22 24
   >: a NB. increment
2 3 4 5 6 7 8 9 10 11 12 13
   >: +: a NB. double then increment
3 5 7 9 11 13 15 17 19 21 23 25
   >:@+: a NB. conjoin the two
3 5 7 9 11 13 15 17 19 21 23 25
```

- Both adverbs and conjunctions are modifiers - the difference is whether they
  take one or two arguments
- `"` here takes a verb (`+/`; which is itself a verb created from a verb and
  adverb) and a noun (`1`)
- `@` works like compose, taking two verbs

---

# [Verb trains](https://www.jsoftware.com/papers/fork.htm)

- Sequences of verbs compose automatically
- `v1 v2` is a hook
    - `(v1 v2) y` is `y v1 (v2 y)`
    - `x (v1 v2) y`  is `x v1 (v2 y)`
- `v1 v2 v3` is a fork
    - `(v1 v2 v3) y` is `(v1 y) v2 (v3 y)`
    - `x (v1 v2 v3) y` is `(x v1 y) v2 (x v3 y)`

---

# Fork to calculate mean

```
   ] a =: 1 + i. 12
1 2 3 4 5 6 7 8 9 10 11 12
   (+/ % #) a NB. mean
6.5
   (+/ a) % (# a)
6.5
```

---

# Reading the vocabulary

- [NuVoc on the wiki](https://code.jsoftware.com/wiki/NuVoc)
- Compare to the [K vocabulary](https://kparc.com/k.txt)

---

# Names and optimisation

- [Special Combinations] are specifically optimised patterns
- `c =: 1000 1000 ?@$ 0` is a table with a million random numbers
- `ts =: 6!:2 , 7!:2@]` is a verb to show the time and space usage of a
  J sentence

```
   plus =: +
   insert =: /
   atop =: @
   ravel =: ,
   ts 'plus insert atop ravel c'
0.256105 2944
   ts '+/@, c'
0.003203 1536
   plus insert atop ravel f.
+/@,
   ts 'plus insert atop ravel f. c'
0.002319 8128
```

[Special Combinations]: https://code.jsoftware.com/wiki/Vocabulary/SpecialCombinations

---

# More resources

- J
    - [Vocabulary](https://code.jsoftware.com/wiki/NuVoc)
    - [Learning J](https://www.jsoftware.com/help/learning/contents.htm)
    - [J for C Programmers](https://www.jsoftware.com/help/jforc/contents.htm)
    - [The first J interpreter](https://code.jsoftware.com/wiki/Essays/Incunabulum) -
      a single page of C
- APL
    - [TryAPL](https://tryapl.org)
    - [Co-dfns](https://github.com/Co-dfns/Co-dfns) - APL compiler
      written in Dyalog APL

---

# More resources

- K
    - [Shakti K tutorial](https://shakti.com/tutorial/)
    - [oK](https://github.com/JohnEarnest/ok) - a JavaScript
      implementation of K (without views)
    - [Lisp comparison][lisp]
    - [Views][views] work like spreadsheet cells
        - [Text editor in K][edit.k] ([HN explanation][hn])
- [no stinking loops](http://nsl.com) - general array programming links

[edit.k]: http://www.kparc.com/edit.k
[hn]: https://news.ycombinator.com/item?id=8476633
[views]: https://code.kx.com/v2/learn/views/
[lisp]: https://github.com/KxSystems/kdb/blob/master/lisp.txt
