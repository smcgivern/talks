class: center, middle

# How GitLab releases software

Sean McGivern (GitLab)

---

class: center, middle

# What is GitLab?

---

# What is GitLab?

* 2011: started as an open-source project
    * '[init commit]'
* 2012: GitLab.com
* 2014: [GitLab B.V.]
* 2018: 300+ people, 40+ countries
* [More history available]

[init commit]: https://gitlab.com/gitlab-org/gitlab-ce/commit/9ba1224867665844b117fa037e1465bb706b3685
[GitLab B.V.]: https://about.gitlab.com/2014/04/18/gitlab-cloud-becomes-gitlab-com/
[More history available]: https://about.gitlab.com/history/

---

# What is GitLab?

* Software for developing and deploying software
* OSS Core, proprietary tiers above that
* Deploy as a [package]
    ```shell
    # Add repository
    $ apt-get install gitlab-ee
    ```
* Deploy as a [Helm chart]
    ```shell
    $ helm repo add gitlab https://charts.gitlab.io/
    $ helm update
    $ helm upgrade --install gitlab gitlab/gitlab
    ```
* Don't deploy at all - use GitLab.com

[package]: https://about.gitlab.com/installation/
[Helm chart]: https://docs.gitlab.com/ee/install/kubernetes/gitlab_chart.html

---

class: center, middle

# Releasing GitLab

---

# Releasing a package

* [Monthly on the 22nd]
* [Patch] only the latest version
    * Security patches go back two versions more
* Supports PostgreSQL + MySQL
* By default:
    * `apt-get upgrade`
    * Instance goes down
    * Run database migrations
    * Instance comes up

[Monthly on the 22nd]: https://about.gitlab.com/releases/
[Patch]: https://docs.gitlab.com/ee/policy/maintenance.html#patch-releases

---

# Deploying to GitLab.com

[![](gitlab-com-architecture.png)][production-architecture]

[production-architecture]: https://about.gitlab.com/handbook/engineering/infrastructure/production-architecture/#infra-current-archi-diagram

---

# Deploying to GitLab.com

* Rails application goes to git, API, Sidekiq, and web nodes
* Deploy to those nodes
* What if more than one tries to run migrations?
    * [Add option to disable migrations completely] \[2016\]
    * Designate a 'blessed node' to run those migrations
* For now, let's simplify and just focus on a few things we got wrong

[Add option to disable migrations completely]: https://gitlab.com/gitlab-org/omnibus-gitlab/issues/1345

---

# Deploying to GitLab.com

* Rails application goes to git, API, Sidekiq, and web nodes
* **And** the blessed node
* Blessed node goes first
    * Can't run the new code without migrations
* Blessed node serves web traffic
    * Therefore it has new assets, with different hashes
    * Each request can go to any web node
    * So assets don't load
    * [Mismatch in assets on different nodes] \[2017\]

[Mismatch in assets on different nodes]: https://gitlab.com/gitlab-com/infrastructure/issues/1396

---

# Deploying to GitLab.com

* Let's assume that we solved that
    * (A dedicated node, for instance)
* That only solves _where_ migrations run, not _how_
* Throughout this time, we're seeing traffic increase a lot
    * I don't have traffic / usage numbers I can share
    * 2017: four web nodes, one database node
    * 2018: fourteen web nodes, four database nodes
    * See our public [fleet overview] for details

[fleet overview]: https://monitor.gitlab.net/d/000000003/fleet-overview?refresh=5m&orgId=1

---

class: center, middle

# Database migrations

---

# Database migrations

* Cannot require downtime
* Best practice: do migrations in small steps
    * Add backwards-compatible code
    * Perform migration (slowly if needed)
    * Remove backwards-compatible code
* Restrict this to Postgres only
    * We run GitLab.com on Postgres
    * Supporting two databases is painful in general, more so for sensitive
      areas

---

# Database migrations

* Cannot require downtime
* Best practice: do migrations in small steps
    * Add backwards-compatible code
    * **Perform migration (slowly if needed)**
    * **Remove backwards-compatible code**

...

* We can deploy to GitLab.com whenever we like (in theory)
* We do not have that control over package users
    * They might upgrade multiple versions at once

---

# Database migrations

* Let's remove an index!

    > A normal `DROP INDEX` acquires exclusive lock on the table, blocking other
    > accesses until the index drop can be completed

* Solution: [`DROP INDEX CONCURRENTLY`]

    > Drop the index without locking out concurrent selects, inserts, updates, and
    > deletes on the index's table

[`DROP INDEX CONCURRENTLY`]: https://www.postgresql.org/docs/9.6/static/sql-dropindex.html

---

# Database migrations

* Let's add a foreign key!
* Similar problem: adding and validating a foreign key will lock both tables
* Solution:

    ```sql
    ALTER TABLE ... ADD CONSTRAINT ... NOT VALID ...;

    -- Then:

    ALTER TABLE ... VALIDATE CONSTRAINT ...;
    ```

    > [If the constraint is marked `NOT VALID`], the potentially-lengthy initial
    > check to verify that all rows in the table satisfy the constraint is skipped

[If the constraint is marked `NOT VALID`]: https://www.postgresql.org/docs/9.6/static/sql-altertable.html

---

# Database migrations

* So what's wrong with those migrations?
* They run before deploy completes
    * The longer the migration, the longer the deployment time
    * Doesn't work well with deploy, fix, deploy models

---

class: center, middle

# Post-deployment migrations

[Post-deployment migrations]: https://docs.gitlab.com/ce/development/post_deployment_migrations.html

---

# Post-deployment migrations

```shell
# Opt-in
$ SKIP_POST_DEPLOYMENT_MIGRATIONS=true gitlab-rake db:migrate
```

---

# Post-deployment migrations

* Removing a column cannot be done without downtime in Rails
* Rails keeps a column cache
* Solution:

    ```ruby
    class Issue < ActiveRecord::Base
      include IgnorableColumn # GitLab mixin
      ignore_column :assignee_id
    end
    ```

    * Deploy this, then drop the column post-deployment
    * Remove this in the next release as a clean-up step

---

# Post-deployment migrations

* So that's all solved, right?
* What if we have a lot of data to change?
    * For instance, let's pretend we stored diff data in a text field as
      serialised Yaml
    * How many files are in the diff? Can't tell without deserialising
    * Do those Yaml objects have the same schema? Can't tell without
      deserialising
    * Can we fetch one specific diff file? Not without deserialising!

---

class: center, middle

# Background migrations

[Background migrations]: https://docs.gitlab.com/ce/development/background_migrations.html

---

# Background migrations

* Use a post-deployment migration to schedule Sidekiq workers
* These workers process the data in batches to smooth out updates
    * Helps with load
    * Helps with deployment times
    * Helps with replication lag
* Target no more than a week on GitLab.com

---

# Background migrations

* Things start to get more tricky here
* Package users don't update to intermediate releases
    * So when we're done, we have to have a [clean-up step]
    * This is not in a patch release, in a monthly release
    * This runs the migration in the foreground, which will be slow
* Don't update immediately X.1 -> X.2 -> X.3 if you want to deploy quickly
    * If the background migration takes a day to run in the foreground, the
      migrations will take a day to run
* Sidekiq jobs can fail!
    * In clean-up, first take all remaining jobs from the queue
    * Then double-check the table is in the state we expect

[clean-up step]: https://docs.gitlab.com/ce/development/background_migrations.html#cleaning-up

---

# Background migrations

* `merge_request_diffs` has an `st_diffs` column (that wasn't a hypothetical 😞)
* This column contains Yaml data - normally hashes

    ```
            Table "public.merge_request_diff_files"
            Column         |       Type        | Modifiers
    -----------------------+-------------------+-----------
     merge_request_diff_id | integer           | not null
     relative_order        | integer           | not null
     new_file              | boolean           | not null
     renamed_file          | boolean           | not null
     deleted_file          | boolean           | not null
     too_large             | boolean           | not null
     a_mode                | character varying | not null
     b_mode                | character varying | not null
     new_path              | text              | not null
     old_path              | text              | not null
     diff                  | text              | not null
     binary                | boolean           |
    ```

---

# Background migrations


```ruby
class MergeRequestDiff
  def diffs_from_database
    if st_diffs.present?
      if valid_raw_diff?(st_diffs)
        st_diffs
      end
    elsif merge_request_diff_files.present?
      merge_request_diff_files
        .as_json(only: Gitlab::Git::Diff::SERIALIZE_KEYS)
        .map(&:with_indifferent_access)
    end
  end
end
```

---

# Background migrations

```ruby
def perform(start_id, stop_id)
  merge_request_diffs = MergeRequestDiff
                          .select(:id, :st_commits, :st_diffs)
                          .where('st_diffs IS NOT NULL')
                          .where(id: start_id..stop_id)

  reset_buffers!

  merge_request_diffs.each do |merge_request_diff|
    diff_ids << merge_request_diff.id

    file_rows.concat(single_diff_rows(merge_request_diff))

    if diff_ids.length > BUFFER_ROWS || # 1000 rows
        file_rows.length > DIFF_FILE_BUFFER_ROWS # 100 files

      # Don't perform too big an INSERT in one chunk
      flush_buffers!
    end
  end

  # Add any remaining rows below the buffer size
  flush_buffers!
end
```

---

# In conclusion

* Deploying software is hard
* It's even harder with multiple ways of delivering the software
* I've mostly been talking about database changes and I'm already out of time
* We haven't solved these problems!
* For instance: maybe use threads in the migration and just run a bunch of
  updates in parallel?
    * [WIP: parallel DB migrations]
    * Auto-pauses when replication lag gets too high
    * Not merged (yet)

[WIP: parallel DB migrations]: https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/20695

---

class: center, middle

# Thank you

[smcgivern.gitlab.io/talks/gitlab-release-process](https://smcgivern.gitlab.io/talks/gitlab-release-process/)
